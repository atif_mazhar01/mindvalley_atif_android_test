package com.cachelib.cachelib;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Created by Atif.dev on 27-Aug-16.
 */

@RunWith(MockitoJUnitRunner.class)
public class MemoryCacheTest {


    MemoryCache memoryCache;
    @Before
    public void setUp() throws Exception {

        memoryCache = new MemoryCache();

    }


    void showMemorySize(){
        Mockito.when(memoryCache.getSize()).thenReturn(1000L);

    }
}
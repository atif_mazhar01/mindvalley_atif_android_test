package com.cachelib.cachelib;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

/**
 * Created by Atif.dev on 28-Aug-16.
 */
public class JSONCacheFileTest {


    JSONCacheFile jsonCacheFile;
    Context mContext;

    final String TEST_STR ="test_string";
    final String TEST_DATA ="test_data";

    @Before
    public void setUp() throws Exception {

        jsonCacheFile = new JSONCacheFile(mContext);
    }

    @Test
    public void putData() throws Exception {

        jsonCacheFile.putData(TEST_STR,TEST_DATA );

    }

    @Test
    public void getCacheData() throws Exception {

        Mockito.when(jsonCacheFile.getCacheData(TEST_STR)).thenReturn(TEST_DATA);

//        Mockito.verify(jsonCacheFile).getCacheData(TEST_STR);

    }

}
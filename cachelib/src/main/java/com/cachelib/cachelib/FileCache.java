package com.cachelib.cachelib;


import java.io.File;
import android.content.Context;
import android.widget.ImageView;

public class FileCache {

    private File cacheDir;

    public FileCache(Context context) {

        cacheDir = context.getCacheDir();
        if (!cacheDir.exists())
            cacheDir.mkdirs();
    }

    public File getFile(String url) {
        //I identify images by hashcode. Not a perfect solution, good for the demo.
        String filename = String.valueOf(url.hashCode());
        //Another possible solution (thanks to grantland)
        //String filename = URLEncoder.encode(url);
        File f = new File(cacheDir, filename);
        return f;

    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null)
            return;
        for (File f : files)
            f.delete();
    }


    public void loadImage(String url, ImageView imageView) {
    }

    public void cacheJSON(String url, String data) {

    }

    public String getCacheData(String url) {
        return null;
    }

}
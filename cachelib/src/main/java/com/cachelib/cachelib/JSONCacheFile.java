package com.cachelib.cachelib;

import android.content.Context;


import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * cache JSON data
 */

public class JSONCacheFile extends FileCache {

    private Map<String, String> cache= Collections.synchronizedMap(
            new LinkedHashMap<String, String>(10,1.5f,true));//Last argument true for LRU ordering


    public JSONCacheFile(Context context){
        super(context);
    }

    public void cacheJSON(String url, String data){
        File fil = getFile(url);

        try{
            // Open the file that is the first
            // command line parameter
            FileOutputStream fstream = new FileOutputStream(fil);
            // Get the object of DataInputStream
            DataOutputStream in = new DataOutputStream(fstream);
            BufferedWriter br = new BufferedWriter(new OutputStreamWriter(in));


            br.write(data);
            br.flush();

            br.close();
            in.close();
        }catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }


    public String getCacheData(String url){

        File fil = getFile(url);

        String data = null;

        try {

            StringBuffer stringBuffer = new StringBuffer();
            Scanner scanner = new Scanner(fil);
            while (scanner.hasNextLine()) {
                stringBuffer.append(scanner.nextLine());
            }

            data = stringBuffer.toString();
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return  data;
    }

}

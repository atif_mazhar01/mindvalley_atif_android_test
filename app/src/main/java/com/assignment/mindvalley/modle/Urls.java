package com.assignment.mindvalley.modle;


import org.json.JSONObject;

public class Urls {

    private String small, raw, full, regular, thumb;



    public Urls(JSONObject jsor) {

        raw = jsor.optString("raw");
        full = jsor.optString("full");
        small = jsor.optString("small");
        regular  = jsor.optString("regular");
        thumb  = jsor.optString("thumb");
    }

    public String getSmall() {
        return small;
    }

    public String getRaw() {
        return raw;
    }

    public String getFull() {
        return full;
    }

    public String getRegular() {
        return regular;
    }
    public String getThumb() {
        return thumb;
    }


}

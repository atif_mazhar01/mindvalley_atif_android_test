package com.assignment.mindvalley.modle;

import android.util.Log;

import com.assignment.mindvalley.web_server.Config;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Map;


public class ImageFeed implements Serializable{


    private String id, created_at, color;//, email, profileImage, status, name;

    private User user;
    private Urls urls;
    private int width, height, likes;
    private boolean liked_by_user;

    public ImageFeed(JSONObject json) {

        id = json.optString("id");
        color = json.optString("color");
        created_at  = json.optString("created_at");
        likes  = json.optInt("likes");
        width =  json.optInt("width");
        height  = json.optInt("height");
        liked_by_user  = json.optBoolean("liked_by_user");

        if(json.has("user")){
            JSONObject juser =  json.optJSONObject("user");
            user = new User(juser);
        }

        if(json.has("urls")){
            JSONObject url =  json.optJSONObject("urls");
            urls = new Urls(url);
        }

    }

    public String getId() {
        return id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getColor() {
        return color;
    }

    public User getUser() {
        return user;
    }

    public Urls getUrls() {
        return urls;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getLikes() {
        return likes;
    }

    public boolean isLiked_by_user() {
        return liked_by_user;
    }
}



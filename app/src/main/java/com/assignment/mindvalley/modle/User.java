package com.assignment.mindvalley.modle;

import android.util.Log;

import com.assignment.mindvalley.web_server.Config;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by atifmazhar on 4/5/16.
 */
public class User implements Serializable{


    private String id, username, name;

    private  ProfileImage profileImage;

    public User(JSONObject juser) {

        id = juser.optString("id");
        username  = juser.optString("username");
        name  = juser.optString("name");

        if(juser.has("profile_image")){
            profileImage  = new ProfileImage(juser.optJSONObject("profile_image"));
        }
   }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public ProfileImage getProfileImage() {
        return profileImage;
    }
}

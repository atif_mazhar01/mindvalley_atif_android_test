package com.assignment.mindvalley.modle;

import android.util.Log;

import org.json.JSONObject;


public class ProfileImage {


    private String small, medium, large;


    public ProfileImage(JSONObject juser) {

        small = juser.optString("small");
        medium = juser.optString("medium");
        large = juser.optString("large");

    }

    public String getSmall() {
        return small;
    }

    public String getMedium() {
        return medium;
    }

    public String getLarge() {
        return large;
    }
}

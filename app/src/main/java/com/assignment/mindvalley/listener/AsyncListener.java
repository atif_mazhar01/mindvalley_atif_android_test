package com.assignment.mindvalley.listener;


import com.assignment.mindvalley.modle.TaskResult;


public interface AsyncListener {

    public void onComplete(TaskResult result);

}

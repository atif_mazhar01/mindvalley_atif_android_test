package com.assignment.mindvalley.parser;

import com.assignment.mindvalley.modle.TaskResult;

/**
 * Created by atifmazhar on 4/5/16.
 */
public interface BaseParser {

    public TaskResult parse(String response);

}

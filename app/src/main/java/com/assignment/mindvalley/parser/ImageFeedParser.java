package com.assignment.mindvalley.parser;


import com.assignment.mindvalley.modle.ImageFeed;
import com.assignment.mindvalley.modle.TaskResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ImageFeedParser implements BaseParser {

	public TaskResult parse(String response) {
		TaskResult result = new TaskResult();
		try {

			ArrayList<ImageFeed> arrList = new ArrayList<ImageFeed>();

			JSONArray jsonArray = new JSONArray(response);
			for(int i=0; i< jsonArray.length(); i++){

				JSONObject json = (JSONObject) jsonArray.get(i);
				ImageFeed imageFeed = new ImageFeed(json);
				arrList.add(imageFeed);
			}
			result.setData(arrList);
			result.success(true);
		} catch (Exception e) {
			result.success(false);
			e.printStackTrace();
		}
		return result;
	}

}

package com.assignment.mindvalley.activity;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.assignment.mindvalley.R;
import com.assignment.mindvalley.adapter.FeedAdapter;
import com.cachelib.cachelib.JSONCacheFile;
import com.assignment.mindvalley.utils.Utils;
import com.assignment.mindvalley.listener.AsyncListener;
import com.assignment.mindvalley.modle.ImageFeed;
import com.assignment.mindvalley.modle.TaskResult;
import com.assignment.mindvalley.parser.ImageFeedParser;
import com.assignment.mindvalley.web_server.Config;
import com.assignment.mindvalley.web_server.WebAPI;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    ListView mListView;
    ArrayList<ImageFeed> mFeeds;

    FeedAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.listView);

        mFeeds = new ArrayList<ImageFeed>();

        findViewById(R.id.fab).setOnClickListener(this);

        String data = new JSONCacheFile(this).getCacheData(Config.API_SAMPLE);
        if( data == null) {

            if(Utils.isNetworkAvaliable(this)) {
                WebAPI.webserverAPI(this, new AsyncListener() {
                    @Override
                    public void onComplete(TaskResult result) {

                        mFeeds.addAll((ArrayList<ImageFeed>) result.getData());
                        handler.sendEmptyMessage(1);
                    }
                });
            }
        }else{

            TaskResult result =  new ImageFeedParser().parse(data);
            mFeeds.addAll((ArrayList<ImageFeed>) result.getData());
            handler.sendEmptyMessage(1);
        }

        adapter = new FeedAdapter(this, mFeeds);
        mListView.setAdapter(adapter);

    }

    Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if(msg.what == 1 )
            {
                adapter.notifyDataSetChanged();
                adapter.notifyDataSetInvalidated();
            }
        }
    };


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.fab:
                Toast.makeText(this, "Click on FloatingActionButton", Toast.LENGTH_SHORT).show();
                break;
        }

    }
}

package com.assignment.mindvalley.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.assignment.mindvalley.R;
import com.cachelib.cachelib.ImageLoader;
import com.assignment.mindvalley.modle.ImageFeed;

import java.util.List;

public class FeedAdapter extends ArrayAdapter<ImageFeed> {

    List<ImageFeed> mListFeeds;
    Context mContext;

    /**
     * cacheImages
     */
    ImageLoader imageLoader;
    public FeedAdapter(Context context, List<ImageFeed> feeds) {
        super(context, 0, feeds);

        mContext = context;
        mListFeeds = feeds;

        imageLoader = new ImageLoader(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ViewHolder holder= null;
        if(view == null){
            view = LayoutInflater.from(mContext).inflate(R.layout.row_post_feeds, null, false);

            holder = new ViewHolder();

            holder.imageView = (ImageView) view.findViewById(R.id.img);

            view.setTag(holder);
        }
        else{

            holder = (ViewHolder) view.getTag();
        }

        imageLoader.loadImage(mListFeeds.get(position).getUrls().getRegular(), holder.imageView);

        return view;
    }

    class ViewHolder{
        ImageView imageView;
    }

}

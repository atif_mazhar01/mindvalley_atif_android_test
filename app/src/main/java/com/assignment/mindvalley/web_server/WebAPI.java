package com.assignment.mindvalley.web_server;

import android.content.Context;
import android.os.AsyncTask;

import com.cachelib.cachelib.FileCache;
import com.assignment.mindvalley.listener.AsyncListener;
import com.assignment.mindvalley.parser.BaseParser;
import com.assignment.mindvalley.parser.ImageFeedParser;
import com.cachelib.cachelib.JSONCacheFile;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;


public class WebAPI {

    public static void webserverAPI(Context context, AsyncListener listener) {

        serverCall(context, Config.API_SAMPLE , null,new ImageFeedParser(), listener);
    }


    public static void serverCall(Context context, String myUrl, RequestBody requestBody, BaseParser parser, AsyncListener listener) {

        APIAsyc apiCall = new APIAsyc(context, myUrl, requestBody, parser, listener);
        apiCall.execute();
    }


    static class APIAsyc extends AsyncTask<Void, Void, String>{

        Context context;
        String url;
        RequestBody requestBody;
        BaseParser parser;
        AsyncListener listener;
        FileCache fileCache;

        public APIAsyc(Context contex, String myUrl, RequestBody requestBody,BaseParser parser, AsyncListener listener){
            this.context = contex;
            url = myUrl;
            this.requestBody = requestBody;
            this.parser= parser;
            this.listener = listener;
            fileCache = new FileCache(contex);
        }

        @Override
        protected String doInBackground(Void... params) {

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            String responseString="";
            try {
                Response response = client.newCall(request).execute();
                responseString = response.body().string();

                response.body().close();
                // do whatever you need to do with responseString
            }
            catch (Exception e) {
                e.printStackTrace();
                return "Oop. something went worng.";
            }
            return responseString;
        }


        @Override
        protected void onPostExecute(String responseString) {
            super.onPostExecute(responseString);

            new JSONCacheFile(context).cacheJSON(url, responseString);

            listener.onComplete(parser.parse(responseString));
        }
    }

}

package com.assignment.mindvalley.utils;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;


@RunWith(MockitoJUnitRunner.class)
public class UtilsTest {

    Context mContext;
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void isNetworkAvaliable() throws Exception {

        Mockito.when(Utils.isNetworkAvaliable(mContext)).thenReturn(true);

    }

}